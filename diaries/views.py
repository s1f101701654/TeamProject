from django.shortcuts import render, redirect
from selenium import webdriver
from django.http import HttpResponse
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.chrome.options import Options
from diaries.models import haru,aki,IP
import time
import re
import datetime

def index(request):
    if request.method=='POST':
        ID=request.POST.get("idname")
        PW=request.POST.get("pswd")
        test = IP(IDname=ID, password=PW,id=1)
        test.save()

    return render(request, 'diaries/index.html')
def toyonetace(request):
    driver= webdriver.Chrome('diaries/drivers/chromedriver.exe')
    driver.get('https://www.ace.toyo.ac.jp/')

    element = driver.find_element_by_name('userid')
    element.send_keys("your id")

    k=driver.find_element_by_name('password')
    k.send_keys("your pswd")

    s=driver.find_element_by_name('login')
    s.click()

    return HttpResponse()


def toyonetg(request):
  now = datetime.datetime.now()
  user_data = IP.objects.get(id=1)
  try:
    # options = Options()
    # options.binary_location = "C:/Program Files (x86)/Google/Chrome/Application/chrome.exe"
    # options.add_argument("--headless")
    # driver= webdriver.Chrome(chrome_options=options, executable_path='C:/Users/Suzuki Kaito/Desktop/chromedriver.exe')
    driver= webdriver.Chrome('diaries/drivers/chromedriver.exe')
    driver.get('https://g-sys.toyo.ac.jp/portal/')

    element = driver.find_element_by_name("j_username")
    element.send_keys(user_data.IDname)

    k=driver.find_element_by_name("j_password")
    k.send_keys(user_data.password)

    s=driver.find_element_by_name('login')
    s.click()

    e=driver.find_element_by_id("menu_3800")
    e.click()

    link = driver.find_element_by_link_text('履修登録確認表')
    link.click()

    time.sleep(1)

    driver.switch_to.window(driver.window_handles[1])

    harugetu = []
    haruka = []
    harusui = []
    harumoku = []
    harukin = []
    harudo = []

    akigetu = []
    akika = []
    akisui = []
    akimoku = []
    akikin = []
    akido = []


    haru_data = []
    aki_data = []

    get_haru = [harugetu, haruka, harusui, harumoku, harukin, harudo]
    get_aki = [akigetu, akika, akisui, akimoku, akikin, akido]


    data = []
    get_data = [get_haru, get_aki]

    for tr in range(2,50):
      xpath = '//*[@id="IN080110"]/form/table/tbody/tr['+str(tr)+']'

      try:
        line_string = driver.find_element_by_xpath(xpath).text
        index = 0
        if re.search(r"^[月火水木金土]", line_string):
          youbi = line_string[index]
          line_string = line_string[2:]
          target_array = line_string.split(" ")
          data.append(youbi)
          data.append(line_string[0])
        else:
          target_array = line_string.split(" ")
          data.append(line_string[0])

  #      exec("%s = %d" % ("data"+str(tr),))

      except NoSuchElementException:
        break

    driver.quit()

  #春データと秋データの分割

    t = 0
    for i in data:
      if i != "秋":
        haru_data.append(i)
      if i == "秋":
        break
      t += 1

    for c in range(t,len(data)):
      aki_data.append(data[c])

    del haru_data[0]
    del aki_data[0]


  #曜日での分割
  #春分割
    k = 0
    for haru in haru_data:
      if haru in "月":
        t = 0
        for i in range(k+1,len(haru_data)):
          if haru_data[i] in ["月","火" , "水" , "木" , "金", "土"]:
            break
          harugetu.append(haru_data[i])
          t += 1
        k += t + 1

      if haru in "火":
        t = 0
        for i in range(k+1,len(haru_data)):
          if haru_data[i] in ["月","火" , "水" , "木" , "金", "土"]:
            break
          haruka.append(haru_data[i])
          t += 1
        k += t + 1

      if haru in "水":
        t = 0
        for i in range(k+1,len(haru_data)):
          if haru_data[i] in ["月","火" , "水" , "木" , "金", "土"]:
            break
          harusui.append(haru_data[i])
          t += 1
        k += t + 1

      if haru in "木":
        t = 0
        for i in range(k+1,len(haru_data)):
          if haru_data[i] in ["月","火" , "水" , "木" , "金", "土"]:
            break
          harumoku.append(haru_data[i])
          t += 1
        k += t + 1

      if haru in "金":
        t = 0
        for i in range(k+1,len(haru_data)):
          if haru_data[i] in ["月","火" , "水" , "木" , "金", "土"]:
            break
          harukin.append(haru_data[i])
          t += 1
        k += t + 1

      if haru in "土":
        t = 0
        for i in range(k+1,len(haru_data)):
          if haru_data[i] in ["月","火" , "水" , "木" , "金", "土"]:
            break
          harudo.append(haru_data[i])
          t += 1
        k += t + 1

      if haru in ["１","２","３","４","５","６"]:
        pass

    print("現在時刻から処理を決定")
  #秋分割
    K = 0
    for _aki in aki_data:
      if _aki in "月":
        t = 0
        for i in range(K+1,len(aki_data)):
          if aki_data[i] in ["月","火" , "水" , "木" , "金", "土"]:
            break
          akigetu.append(aki_data[i])
          t += 1
        K += t + 1

      if _aki in "火":
        t = 0
        for i in range(K+1,len(aki_data)):
          if aki_data[i] in ["月","火" , "水" , "木" , "金", "土"]:
            break
          akika.append(aki_data[i])
          t += 1
        K += t + 1

      if _aki in "水":
        t = 0
        for i in range(K+1,len(aki_data)):
          if aki_data[i] in ["月","火" , "水" , "木" , "金", "土"]:
            break
          akisui.append(aki_data[i])
          t += 1
        K += t + 1

      if _aki in "木":
        t = 0
        for i in range(K+1,len(aki_data)):
          if aki_data[i] in ["月","火" , "水" , "木" , "金", "土"]:
            break
          akimoku.append(aki_data[i])
          t += 1
        K += t + 1

      if _aki in "金":
        t = 0
        for i in range(K+1,len(aki_data)):
          if aki_data[i] in ["月","火" , "水" , "木" , "金", "土"]:
            break
          akikin.append(aki_data[i])
          t += 1
        K += t + 1

      if _aki in "土":
        t = 0
        for i in range(K+1,len(aki_data)):
          if aki_data[i] in ["月","火" , "水" , "木" , "金", "土"]:
            break
          akido.append(aki_data[i])
          t += 1
        K += t + 1

      if _aki in ["１","２","３","４","５","６"]:
        pass

    print("現在時刻から処理を決定")
  #現在時間から処理を決定
    if 4 <= now.month and now.month <= 7:
      print("haru")
      direction = haru.objects.get(id=1)
      for s in range(0,6):
        last_data = get_data[0][s]
        if last_data == harugetu:
          for i in range(0,len(last_data)):
            if last_data[i] == "１":
              direction.getu1 += 1
            if last_data[i] == "２":
              direction.getu2 += 1
            if last_data[i] == "３":
              direction.getu3 += 1
            if last_data[i] == "４":
              direction.getu4 += 1
            if last_data[i] == "５":
              direction.getu5 += 1
            if last_data[i] == "６":
              direction.getu6 += 1

        if last_data == haruka:
          for i in range(0,len(last_data)):
            if last_data[i] == "１":
              direction.ka1 += 1
            if last_data[i] == "２":
              direction.ka2 += 1
            if last_data[i] == "３":
              direction.ka3 += 1
            if last_data[i] == "４":
              direction.ka4 += 1
            if last_data[i] == "５":
              direction.ka5 += 1
            if last_data[i] == "６":
              direction.ka6 += 1

        if last_data == harusui:
          for i in range(0,len(last_data)):
            if last_data[i] == "１":
              direction.sui1 += 1
            if last_data[i] == "２":
              direction.sui2 += 1
            if last_data[i] == "３":
              direction.sui3 += 1
            if last_data[i] == "４":
              direction.sui4 += 1
            if last_data[i] == "５":
              direction.sui5 += 1
            if last_data[i] == "６":
              direction.sui6 += 1

        if last_data == harumoku:
          for i in range(0,len(last_data)):
            if last_data[i] == "１":
              direction.moku1 += 1
            if last_data[i] == "２":
              direction.moku2 += 1
            if last_data[i] == "３":
              direction.moku3 += 1
            if last_data[i] == "４":
              direction.moku4 += 1
            if last_data[i] == "５":
              direction.moku5 += 1
            if last_data[i] == "６":
              direction.moku6 += 1

        if last_data == harukin:
          for i in range(0,len(last_data)):
            if last_data[i] == "１":
              direction.kin1 += 1
            if last_data[i] == "２":
              direction.kin2 += 1
            if last_data[i] == "３":
              direction.kin3 += 1
            if last_data[i] == "４":
              direction.kin4 += 1
            if last_data[i] == "５":
              direction.kin5 += 1
            if last_data[i] == "６":
              direction.kin6 += 1

        if last_data == harudo:
          for i in range(0,len(last_data)):
            if last_data[i] == "１":
              direction.dou1 += 1
            if last_data[i] == "２":
              direction.dou2 += 1
            if last_data[i] == "３":
              direction.dou3 += 1
            if last_data[i] == "４":
              direction.dou4 += 1
            if last_data[i] == "５":
              direction.dou5 += 1
            if last_data[i] == "６":
              direction.dou6 += 1

      data_data = {"mon1":direction.getu1,
      "mon2":direction.getu2,
      "mon3":direction.getu3,
      "mon4":direction.getu4,
      "mon5":direction.getu5,
      "mon6":direction.getu6,
      "tue1":direction.ka1,
      "tue2":direction.ka2,
      "tue3":direction.ka3,
      "tue4":direction.ka4,
      "tue5":direction.ka5,
      "tue6":direction.ka6,
      "wed1":direction.sui1,
      "wed2":direction.sui2,
      "wed3":direction.sui3,
      "wed4":direction.sui4,
      "wed5":direction.sui5,
      "wed6":direction.sui6,
      "thu1":direction.moku1,
      "thu2":direction.moku2,
      "thu3":direction.moku3,
      "thu4":direction.moku4,
      "thu5":direction.moku5,
      "thu6":direction.moku6,
      "fri1":direction.kin1,
      "fri2":direction.kin2,
      "fri3":direction.kin3,
      "fri4":direction.kin4,
      "fri5":direction.kin5,
      "fri6":direction.kin6,
      "sat1":direction.dou1,
      "sat2":direction.dou2,
      "sat3":direction.dou3,
      "sat4":direction.dou4,
      "sat5":direction.dou5,
      "sat6":direction.dou6,
      }

    else:
      print("elseaaaa")
      #direction = aki.objects.get(id=1)
      direction = aki.objects.get(id=1)

      print("direction")
      for s in range(0,6):
        print("for")
        last_data = get_data[1][s]
        print("last_data")
        if s == 0:
          print("s==0")
          for i in range(0,len(last_data)):
            print("for i")
            print("last_data: " + last_data[i])
            if last_data[i] == "１":
              print("test message")
              direction.a_getu1 += 1
            if last_data[i] == "２":
              direction.a_getu2 += 1
            if last_data[i] == "３":
              direction.a_getu3 += 1
            if last_data[i] == "４":
              direction.a_getu4 += 1
            if last_data[i] == "５":
              direction.a_getu5 += 1
            if last_data[i] == "６":
              direction.a_getu6 += 1

        if s == 1:
          for i in range(0,len(last_data)):
            if last_data[i] == "１":
              direction.a_ka1 += 1
            if last_data[i] == "２":
              direction.a_ka2 += 1
            if last_data[i] == "３":
              direction.a_ka3 += 1
            if last_data[i] == "４":
              direction.a_ka4 += 1
            if last_data[i] == "５":
              direction.a_ka5 += 1
            if last_data[i] == "６":
              direction.a_ka6 += 1

        if s == 2:
          for i in range(0,len(last_data)):
            if last_data[i] == "１":
              direction.a_sui1 += 1
            if last_data[i] == "２":
              direction.a_sui2 += 1
            if last_data[i] == "３":
              direction.a_sui3 += 1
            if last_data[i] == "４":
              direction.a_sui4 += 1
            if last_data[i] == "５":
              direction.a_sui5 += 1
            if last_data[i] == "６":
              direction.a_sui6 += 1

        if s == 3:
          for i in range(0,len(last_data)):
            if last_data[i] == "１":
              direction.a_moku1 += 1
            if last_data[i] == "２":
              direction.a_moku2 += 1
            if last_data[i] == "３":
              direction.a_moku3 += 1
            if last_data[i] == "４":
              direction.a_moku4 += 1
            if last_data[i] == "５":
              direction.a_moku5 += 1
            if last_data[i] == "６":
              direction.a_moku6 += 1

        if s == 4:
          for i in range(0,len(last_data)):
            if last_data[i] == "１":
              direction.a_kin1 += 1
            if last_data[i] == "２":
              direction.a_kin2 += 1
            if last_data[i] == "３":
              direction.a_kin3 += 1
            if last_data[i] == "４":
              direction.a_kin4 += 1
            if last_data[i] == "５":
              direction.a_kin5 += 1
            if last_data[i] == "６":
              direction.a_kin6 += 1

        if s == 5:
          for i in range(0,len(last_data)):
            if last_data[i] == "１":
              direction.a_dou1 += 1
            if last_data[i] == "２":
              direction.a_dou2 += 1
            if last_data[i] == "３":
              direction.a_dou3 += 1
            if last_data[i] == "４":
              direction.a_dou4 += 1
            if last_data[i] == "５":
              direction.a_dou5 += 1
            if last_data[i] == "６":
              direction.a_dou6 += 1

      direction.save()
      data_data = {"mon1":direction.a_getu1,
      "mon2":direction.a_getu2,
      "mon3":direction.a_getu3,
      "mon4":direction.a_getu4,
      "mon5":direction.a_getu5,
      "mon6":direction.a_getu6,
      "tue1":direction.a_ka1,
      "tue2":direction.a_ka2,
      "tue3":direction.a_ka3,
      "tue4":direction.a_ka4,
      "tue5":direction.a_ka5,
      "tue6":direction.a_ka6,
      "wed1":direction.a_sui1,
      "wed2":direction.a_sui2,
      "wed3":direction.a_sui3,
      "wed4":direction.a_sui4,
      "wed5":direction.a_sui5,
      "wed6":direction.a_sui6,
      "thu1":direction.a_moku1,
      "thu2":direction.a_moku2,
      "thu3":direction.a_moku3,
      "thu4":direction.a_moku4,
      "thu5":direction.a_moku5,
      "thu6":direction.a_moku6,
      "fri1":direction.a_kin1,
      "fri2":direction.a_kin2,
      "fri3":direction.a_kin3,
      "fri4":direction.a_kin4,
      "fri5":direction.a_kin5,
      "fri6":direction.a_kin6,
      "sat1":direction.a_dou1,
      "sat2":direction.a_dou2,
      "sat3":direction.a_dou3,
      "sat4":direction.a_dou4,
      "sat5":direction.a_dou5,
      "sat6":direction.a_dou6,
      }
    return render(request, 'diaries/index.html',data_data)
  except e:
    print(e)
    return render(request, 'diaries/index.html')
