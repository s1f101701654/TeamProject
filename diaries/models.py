from django.db import models

class User(models.Model):
    """ユーザーのモデル"""
    name = models.CharField(max_length=256)

    def __str__(self):
        return self.name


class Diary(models.Model):
    """日記のモデル"""
    title = models.CharField(max_length=256)
    detail = models.TextField()
    created = models.DateTimeField()
    user = models.ForeignKey(User)

    def __str__(self):
        return self.title

class haru(models.Model):
    getu1=models.IntegerField(default=0)
    getu2=models.IntegerField(default=0)
    getu3=models.IntegerField(default=0)
    getu4=models.IntegerField(default=0)
    getu5=models.IntegerField(default=0)
    getu6=models.IntegerField(default=0)

    ka1=models.IntegerField(default=0)
    ka2=models.IntegerField(default=0)
    ka3=models.IntegerField(default=0)
    ka4=models.IntegerField(default=0)
    ka5=models.IntegerField(default=0)
    ka6=models.IntegerField(default=0)


    sui1=models.IntegerField(default=0)
    sui2=models.IntegerField(default=0)
    sui3=models.IntegerField(default=0)
    sui4=models.IntegerField(default=0)
    sui5=models.IntegerField(default=0)
    sui6=models.IntegerField(default=0)


    moku1=models.IntegerField(default=0)
    moku2=models.IntegerField(default=0)
    moku3=models.IntegerField(default=0)
    moku4=models.IntegerField(default=0)
    moku5=models.IntegerField(default=0)
    moku6=models.IntegerField(default=0)


    kin1=models.IntegerField(default=0)
    kin2=models.IntegerField(default=0)
    kin3=models.IntegerField(default=0)
    kin4=models.IntegerField(default=0)
    kin5=models.IntegerField(default=0)
    kin6=models.IntegerField(default=0)

    dou1=models.IntegerField(default=0)
    dou2=models.IntegerField(default=0)
    dou3=models.IntegerField(default=0)
    dou4=models.IntegerField(default=0)
    dou5=models.IntegerField(default=0)
    dou6=models.IntegerField(default=0)




class aki(models.Model):
    a_getu1=models.IntegerField(default=0)
    a_getu2=models.IntegerField(default=0)
    a_getu3=models.IntegerField(default=0)
    a_getu4=models.IntegerField(default=0)
    a_getu5=models.IntegerField(default=0)
    a_getu6=models.IntegerField(default=0)


    a_ka1=models.IntegerField(default=0)
    a_ka2=models.IntegerField(default=0)
    a_ka3=models.IntegerField(default=0)
    a_ka4=models.IntegerField(default=0)
    a_ka5=models.IntegerField(default=0)
    a_ka6=models.IntegerField(default=0)


    a_sui1=models.IntegerField(default=0)
    a_sui2=models.IntegerField(default=0)
    a_sui3=models.IntegerField(default=0)
    a_sui4=models.IntegerField(default=0)
    a_sui5=models.IntegerField(default=0)
    a_sui6=models.IntegerField(default=0)


    a_moku1=models.IntegerField(default=0)
    a_moku2=models.IntegerField(default=0)
    a_moku3=models.IntegerField(default=0)
    a_moku4=models.IntegerField(default=0)
    a_moku5=models.IntegerField(default=0)
    a_moku6=models.IntegerField(default=0)


    a_kin1=models.IntegerField(default=0)
    a_kin2=models.IntegerField(default=0)
    a_kin3=models.IntegerField(default=0)
    a_kin4=models.IntegerField(default=0)
    a_kin5=models.IntegerField(default=0)
    a_kin6=models.IntegerField(default=0)


    a_dou1=models.IntegerField(default=0)
    a_dou2=models.IntegerField(default=0)
    a_dou3=models.IntegerField(default=0)
    a_dou4=models.IntegerField(default=0)
    a_dou5=models.IntegerField(default=0)
    a_dou6=models.IntegerField(default=0)

class IP(models.Model):
    IDname =models.CharField(max_length=12)
    password=models.CharField(max_length=20)
